import React from "react";
import "./home.css";
import { Link } from "react-router-dom";
import pic from './bg.jpg'
const Home = (props) => {
    const { activeTab, setSearchInput, searchInput } = props;
    // console.log(activeTab);
    const renderSearchContainer = () => {
      return (
        <div className="search-container">
          <input
            className="form-control py-2"
            type="search"
            value={searchInput}
            id="example-search-input"
            placeholder="Search"
            onChange={(event) => setSearchInput(event.target.value)}
          />
          {/* <span className="input-group-append">
            <button
              className="btn btn-primary"
              type="button"
              onClick={() => setSearchInput(inputVal)}
            >
              <i className="fa fa-search">
                <BsSearch />
              </i>
            </button>
          </span> */}
        </div>
      );
    };
    return (
    <>
    
      <div id="headerContainer">
        <ul className="tabs-ul">
          <Link to="/addProducts" style={{ textDecoration: "none" }}>
            <li
              className={`tab-item ${
                activeTab === "ADD PRODUCTS" && "activeTabStyles"
              }`}
            >
              ADD PRODUCTS
            </li>
          </Link>
          <Link to="/products" style={{ textDecoration: "none" }}>
            <li
              className={`tab-item ${
                activeTab === "PRODUCTS" && "activeTabStyles"
              }`}
            >
              PRODUCTS
            </li>
          </Link>
        </ul>
        {activeTab === "PRODUCTS" && renderSearchContainer()}
      </div>
      <img src={pic}/>
      
      </>
    );
  };
  export default Home;